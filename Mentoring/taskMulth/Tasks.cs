﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Linq;
using System.Threading;
using System.Collections.Generic;

namespace taskMulth
{
    [TestClass]
    public class Tasks
    {
        [TestMethod]
        public void Task1()
        {
            var count = 100;
            var tasks = new Task[count];

            for (int i = 0; i < count; i++)
            {
                tasks[i] = Task.Factory.StartNew((taskNumber) =>
                {
                    var length = 1000;
                    for (int j = 1; j <= length; j++)
                    {
                        Console.WriteLine($"Task #{taskNumber} - {j}");
                    }
                }, i);
            }

            Task.WaitAll(tasks);
        }

        [TestMethod]
        public void Task2()
        {
            var taskA = Task.Factory.StartNew(() =>
            {
                var count = 10;
                var array = new int[count];
                var random = new Random();

                Console.WriteLine("Task #1 is going to generate array of number.");

                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = random.Next(0, 50);
                    Console.WriteLine(array[i]);
                }

                return array;
            });

            var taskB = taskA.ContinueWith((result) =>
            {
                var array = result.Result;
                var random = new Random();

                Console.WriteLine("Task #2 is going to multiply array and random number.");

                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = array[i] * random.Next(0, 80);
                    Console.WriteLine(array[i]);
                }

                return array;
            });

            var taskC = taskB.ContinueWith((result) =>
            {
                var array = result.Result;
                array = array.OrderBy(x => x).ToArray();

                Console.WriteLine("Task #3 is going to order array.");

                foreach (var item in array)
                {
                    Console.WriteLine(item);
                }

                return array;
            });

            var TaskD = taskC.ContinueWith((result) =>
            {
                Console.WriteLine("Task #4 is going to calculate the average value.");

                double averageValue = result.Result.Average();

                Console.WriteLine(averageValue);
            });

            TaskD.Wait();
        }

        [TestMethod]
        public void Task3()
        {
            try
            {
                var matrixA = new int[2, 3];
                matrixA[0, 0] = 1;
                matrixA[0, 1] = 2;
                matrixA[0, 2] = 3;
                matrixA[1, 0] = 4;
                matrixA[1, 1] = 5;
                matrixA[1, 2] = 6;

                var matrixB = new int[3, 2];
                matrixB[0, 0] = 7;
                matrixB[0, 1] = 8;
                matrixB[1, 0] = 9;
                matrixB[1, 1] = 10;
                matrixB[2, 0] = 11;
                matrixB[2, 1] = 12;

                Func<int[,], int[,], int[,]> calculate = (int[,] a, int[,] b) =>
                {
                    var aRowCount = a.GetLength(0);
                    var aColumnCount = a.GetLength(1);

                    var bRowCount = b.GetLength(0);
                    var bColumnCount = b.GetLength(1);

                    if (aColumnCount != bRowCount)
                    {
                        throw new ArgumentOutOfRangeException("Matrices can not be multiplied ");
                    }

                    var resultMatrix = new int[aRowCount, bColumnCount];

                    Parallel.For(0, aRowCount, (number, state) =>
                    {
                        for (int j = 0; j < bColumnCount; j++)
                        {
                            int temp = 0;
                            for (int k = 0; k < aColumnCount; k++)
                            {
                                temp += a[number, k] * b[k, j];
                            }

                            resultMatrix[number, j] = temp;
                        }
                    });

                    return resultMatrix;
                };

                var result = calculate(matrixA, matrixB);

                Assert.AreEqual(result[0, 0], 58);
                Assert.AreEqual(result[0, 1], 64);
                Assert.AreEqual(result[1, 0], 139);
                Assert.AreEqual(result[1, 1], 154);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong...");
                Console.WriteLine(ex.ToString());
            }
        }

        [TestMethod]
        public void Task4()
        {
            try
            {
                int threadCount = 10;
                bool isFirstIteration = true;
                Action<int> doDecrement = null;

                doDecrement = (number) =>
                {
                    if (number != 0)
                    {
                        if (!isFirstIteration)
                        {
                            number--;
                            Console.WriteLine(number);
                        }
                        else
                        {
                            isFirstIteration = false;
                        }

                        var threadStart = new ParameterizedThreadStart(x => doDecrement(number));
                        var thread = new Thread(threadStart);

                        thread.Start();
                        thread.Join();
                    }
                };

                doDecrement(threadCount);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong...");
                Console.WriteLine(ex.ToString());
            }
        }

        [TestMethod]
        public void Task5()
        {
            try
            {
                bool isFirstIteration = true;
                int threadCount = 10;
                var semaphore = new Semaphore(1, 1);
                WaitCallback doDecrement = null;

                doDecrement = (number) =>
                {
                    var value = Convert.ToInt32(number);
                    if (value != 0)
                    {
                        semaphore.WaitOne();

                        if (!isFirstIteration)
                        {
                            value--;
                            Console.WriteLine(number);
                        }
                        else
                        {
                            isFirstIteration = false;
                        }

                        ThreadPool.QueueUserWorkItem(doDecrement, value);
                        
                        semaphore.Release();
                    }
                };

                doDecrement(threadCount);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong...");
                Console.WriteLine(ex.ToString());
            }
        }

        [TestMethod]
        public void Task6()
        {
            try
            {
                int count = 10;
                var list = new List<int>();
                var semaphore = new SemaphoreSlim(1, 1);

                var writerThread = new Thread(() =>
                {
                    var random = new Random();

                    while (list.Count < count)
                    {
                        semaphore.Wait();

                        var value = random.Next(100, 10000);
                        list.Add(value);

                        Console.WriteLine($"Number {value} has been inserted in collection");

                        semaphore.Release();

                        //Delay. After it reader will take control of shared collection.
                        Thread.Sleep(100);
                    }
                });

                var readerThread = new Thread(() =>
                {
                    int previousCount = 0;

                    while (list.Count <= count)
                    {
                        semaphore.Wait();

                        if (list.Count > 0 && list.Count != previousCount)
                        {
                            previousCount = list.Count;
                            Console.WriteLine("Collection contains:");

                            foreach (var item in list)
                            {
                                Console.WriteLine(item);
                            }
                        }

                        if (list.Count == count)
                            break;

                        semaphore.Release();
                    }
                });

                writerThread.Start();
                readerThread.Start();

                // Workaround stops current tread while writer and reader are working.
                // Only for MStests.
                while(list.Count != count) { }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong...");
                Console.WriteLine(ex.ToString());
            }
        }


        [TestMethod]
        public void Task7A()
        {
            var taskCompletionSource = new TaskCompletionSource<int>(12);
            taskCompletionSource.SetCanceled();

            var task = taskCompletionSource.Task;

            var continuationTask = task.ContinueWith((taskResult) =>
            {
                Console.WriteLine($"Is parent task canceled: {taskResult.IsCanceled}");
                Console.WriteLine("continuationTask");
            }, TaskContinuationOptions.None);
        }

        [TestMethod]
        public void Task7B()
        {
            var taskCompletionSource = new TaskCompletionSource<int>(12);
            taskCompletionSource.SetCanceled();

            var task = taskCompletionSource.Task;

            var continuationTask = task.ContinueWith((taskResult) => {
                Console.WriteLine($"Is parent task canceled: {taskResult.IsCanceled}");
                Console.WriteLine("continuationTask");
            }, TaskContinuationOptions.OnlyOnCanceled);
        }

        [TestMethod]
        public void Task7C()
        {
            Func<Task> func = () => {
                Console.WriteLine($"ManagedThreadId: {Thread.CurrentThread.ManagedThreadId}");

                var taskCompletionSource = new TaskCompletionSource<int>(12);
                taskCompletionSource.SetException(new Exception("Test"));                

                return taskCompletionSource.Task;
            };          

            var continuationTask = func().ContinueWith((taskResult) => {
                Console.WriteLine($"ManagedThreadId: {Thread.CurrentThread.ManagedThreadId}");
                Console.WriteLine($"Is parent task IsFaulted: {taskResult.IsFaulted}");
                Console.WriteLine("continuationTask");
            }, TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.ExecuteSynchronously);
        }

        [TestMethod]
        public void Task7D()
        {
            Func<Task> func = () => {
                Console.WriteLine($"ManagedThreadId: {Thread.CurrentThread.ManagedThreadId}");

                var taskCompletionSource = new TaskCompletionSource<int>(12);
                taskCompletionSource.SetCanceled();

                return taskCompletionSource.Task;
            };

            var continuationTask = func().ContinueWith((taskResult) => {

                SynchronizationContext.SetSynchronizationContext(new ThreadPoolSynchronizationContext());
                Console.WriteLine($"ManagedThreadId: {Thread.CurrentThread.ManagedThreadId}");

                Console.WriteLine($"Is parent task canceled: {taskResult.IsCanceled}");
                Console.WriteLine("continuationTask");
            }, TaskContinuationOptions.OnlyOnCanceled);
        }
    }
}
