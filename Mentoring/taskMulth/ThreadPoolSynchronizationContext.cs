﻿using System.Threading;

namespace taskMulth
{
    public class ThreadPoolSynchronizationContext : SynchronizationContext
    {
        public override void Post(SendOrPostCallback d, object state)
        {
            WaitCallback callback = (obj) => { d.Invoke(obj); };

            ThreadPool.QueueUserWorkItem(callback, state);
        }
    }
}
