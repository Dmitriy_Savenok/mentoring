﻿using System.Collections.Generic;
using System.Configuration;

namespace Task.Configuration
{
    internal class MonitoredFolderCollection : ConfigurationElementCollection, IEnumerable<MonitoredFolderConfigurationElement>
    {
        private readonly List<MonitoredFolderConfigurationElement> resources = new List<MonitoredFolderConfigurationElement>();

        IEnumerator<MonitoredFolderConfigurationElement> IEnumerable<MonitoredFolderConfigurationElement>.GetEnumerator()
        {
            return resources.GetEnumerator();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            var resource = new MonitoredFolderConfigurationElement();
            resources.Add(resource);
            return resource;
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return resources.Find(e => e.Equals(element));
        }
    }
}
