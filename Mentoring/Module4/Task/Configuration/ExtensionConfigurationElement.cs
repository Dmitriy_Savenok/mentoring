﻿using System.Configuration;

namespace Task.Configuration
{
    internal class ExtensionConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("extension", IsRequired = true)]
        public string Extension
        {
            get { return (string)this["extension"]; }
        }
    }
}
