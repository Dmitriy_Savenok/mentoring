﻿using System.Configuration;

namespace Task.Configuration
{
    internal class MonitoredFolderConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("folder", IsRequired = true)]
        public string Folder
        {
            get { return (string)this["folder"]; }
        }
    }
}
