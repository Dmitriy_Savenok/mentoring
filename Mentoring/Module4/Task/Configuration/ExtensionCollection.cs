﻿using System.Collections.Generic;
using System.Configuration;

namespace Task.Configuration
{
    internal class ExtensionCollection : ConfigurationElementCollection, IEnumerable<ExtensionConfigurationElement>
    {
        private readonly List<ExtensionConfigurationElement> resources = new List<ExtensionConfigurationElement>();

        IEnumerator<ExtensionConfigurationElement> IEnumerable<ExtensionConfigurationElement>.GetEnumerator()
        {
            return resources.GetEnumerator();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            var resource = new ExtensionConfigurationElement();
            resources.Add(resource);
            return resource;
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return resources.Find(e => e.Equals(element));
        }
    }
}
