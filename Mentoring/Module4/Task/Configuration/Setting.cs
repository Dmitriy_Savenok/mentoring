﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Task.Configuration
{
    internal class Setting
    {
        private const string _configSection = "service";

        public Setting()
        {
            var config = (ServiceConfigurationSection)ConfigurationManager.GetSection(_configSection);

            MonitoredFolders = config.MonitoredFolders.Select(x => x.Folder);
            RequiredExtensions = config.RequiredExtensions.Select(x => x.Extension);

            MonitoredInterval = config.MonitoredInterval;
            StorageFolder = config.StorageFolder;
            Timeout = config.Timeout;
        }

        public string StorageFolder { get; set; }

        public double MonitoredInterval { get; set; }

        public double Timeout { get; set; }

        public IEnumerable<string> MonitoredFolders { get; set; }

        public IEnumerable<string> RequiredExtensions { get; set; }
    }
}
