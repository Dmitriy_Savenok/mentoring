﻿using System.Configuration;

namespace Task.Configuration
{
    internal class ServiceConfigurationSection: ConfigurationSection
    {
        [ConfigurationProperty("monitoredFolders", IsRequired = true)]
        [ConfigurationCollection(typeof(MonitoredFolderConfigurationElement))]
        public MonitoredFolderCollection MonitoredFolders
        {
            get { return (MonitoredFolderCollection)this["monitoredFolders"]; }
        }

        [ConfigurationProperty("storageFolder", IsRequired = true)]
        public string StorageFolder
        {
            get { return (string)this["storageFolder"]; }
        }

        [ConfigurationProperty("monitoredInterval", DefaultValue = 5000d, IsRequired = false)]
        public double MonitoredInterval
        {
            get { return (double)this["monitoredInterval"]; }
        }

        [ConfigurationProperty("timeout", DefaultValue = 10000d, IsRequired = false)]
        public double Timeout
        {
            get { return (double)this["timeout"]; }
        }

        [ConfigurationProperty("extensions", IsRequired = true)]
        [ConfigurationCollection(typeof(ExtensionConfigurationElement))]
        public ExtensionCollection RequiredExtensions
        {
            get { return (ExtensionCollection)this["extensions"]; }
        }
    }
}
