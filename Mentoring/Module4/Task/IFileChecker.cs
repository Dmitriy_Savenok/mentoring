﻿using System;

namespace Task
{
    internal interface IFileChecker : IDisposable
    {
        void CheckAndBuildDocument();

        string CheckedFolder { get; }

        void BuildDocument();

        void StopCheck();
    }
}
