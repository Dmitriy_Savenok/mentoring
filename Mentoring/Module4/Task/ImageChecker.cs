﻿using com.LandonKey.OrderByNatural;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace Task
{
    internal class ImageChecker: IFileChecker
    {
        private readonly string _outputFolder;
        private readonly PdfDocumentBuilder _builder;
        private readonly List<string> _extensions = new List<string>();
        private readonly Timer _expirationTimer;

        private string _firstAddedImage = string.Empty;
        private string _lastAddedImage = string.Empty;
        private int _lastAddedImageNumber = 0;
        private int _lastAddedExpirationImageNumber = 0;

        public ImageChecker(string checkedFolder, string outputFolder, IEnumerable<string> extensions, double timeout)
        {
            CheckedFolder = checkedFolder;

            _outputFolder = outputFolder;
            _extensions.AddRange(extensions);
            _builder = new PdfDocumentBuilder();

            _expirationTimer = new Timer(timeout);
            _expirationTimer.Elapsed += new ElapsedEventHandler(onExpirationTimerElapsed);
        }

        public string CheckedFolder { get; private set; }

        public bool IsStopped { get; private set; }

        public void CheckAndBuildDocument()
        {
            if (IsStopped)
                return;

            var files = Directory.EnumerateFiles(CheckedFolder).OrderByNatural(x => x);
            foreach (var file in files)
            {
                var fileName = Path.GetFileName(file);

                var number = GetCheckedImageNumber(fileName);

                if(_lastAddedImageNumber >= number)
                {
                    continue;
                }
                if ((number - _lastAddedImageNumber) == 1) 
                {
                    _builder.AddSection(file);

                    if (_lastAddedImageNumber == 0)
                    {
                        _firstAddedImage = file;
                    }
                }
                else 
                {
                    BuildDocument();

                    _builder.AddSection(file);

                    _firstAddedImage = file;                   
                }

                _lastAddedImage = file;
                _lastAddedImageNumber = number;
                _lastAddedExpirationImageNumber = number;
                _expirationTimer.Start();
            }            
        }

        private int GetCheckedImageNumber(string fileName)
        {
            var number = 0;

            if (!_extensions.Contains(Path.GetExtension(fileName)))
                throw new ArgumentOutOfRangeException($"Image {fileName} has incorrect format");

            var shortFileName = Path.GetFileNameWithoutExtension(fileName);
            shortFileName = shortFileName.Split('_').Last();

            if (!int.TryParse(shortFileName, out number)) 
                throw new ArgumentOutOfRangeException($"Image {fileName} has incorrect format");

            return number;
        }

        public string CreateFileName()
        {
            var fileName = string.Empty;

            if (!string.IsNullOrEmpty(_lastAddedImage) && !string.IsNullOrEmpty(_firstAddedImage))
            {
                var folder = Path.GetFileName(CheckedFolder);
                var first = Path.GetFileNameWithoutExtension(_firstAddedImage);
                var last = Path.GetFileNameWithoutExtension(_lastAddedImage);

                fileName = $"\\{folder}-{first}-{last}.pdf";
            }
            else
            {
                throw new ArgumentNullException($"Field {nameof(_firstAddedImage)} or {nameof(_firstAddedImage)} can not be null or empty");
            }

            return fileName;
        }

        public void BuildDocument()
        {
            if (IsStopped)
                return;

            if (!_builder.IsEmpty())
            {
                _builder.Save(_outputFolder + CreateFileName());
                _builder.Clear();
            }
        }

        public void Dispose()
        {
            _expirationTimer.Dispose();
            _builder.Clear();
            _extensions.Clear();
        }

        public void StopCheck()
        {
            _expirationTimer.Stop();
            IsStopped = true;
        }

        public void onExpirationTimerElapsed(object sender, ElapsedEventArgs args)
        {
            if (_lastAddedImageNumber == _lastAddedExpirationImageNumber)
            {
                BuildDocument();
            }
        }
    }
}
