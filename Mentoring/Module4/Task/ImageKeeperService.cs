﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Task.Configuration;
using Topshelf;
using Tasks = System.Threading.Tasks;

namespace Task
{
    public class ImageKeeperService : ServiceControl, IDisposable
    {
        private readonly Setting _setting;
        private readonly Timer _monitoringTimer;
        
        private readonly List<IFileChecker> _checkers;

        public ImageKeeperService()
        {
            _setting = new Setting();

            _monitoringTimer = new Timer(_setting.MonitoredInterval);
            _monitoringTimer.Elapsed += new ElapsedEventHandler(onMonitoringTimerElapsed);

            _checkers = new List<IFileChecker>();
            _checkers.AddRange(_setting.MonitoredFolders.Select(folder => new ImageChecker(
                folder, _setting.StorageFolder, _setting.RequiredExtensions, _setting.Timeout)));
        }

        public void Dispose()
        {
            _monitoringTimer.Dispose();

            foreach (var checker in _checkers)
            {
                checker.Dispose();
            }
        }

        public bool Start(HostControl hostControl)
        {
            _monitoringTimer.Start();

            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _monitoringTimer.Stop();

            return true;
        }

        public void onMonitoringTimerElapsed(object sender, ElapsedEventArgs args)
        {
            Tasks.Parallel.ForEach(_checkers, (checker) => { checker.CheckAndBuildDocument(); });
        }
    }
}
