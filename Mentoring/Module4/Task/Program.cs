﻿using Topshelf;

namespace Task
{
    public static class Program
    {
        public static void Main()
        {
            int firstRecoveryDelay = 1;
            int secondRecoveryDelay = 5;

            HostFactory.Run(x =>
            {
                x.Service<ImageKeeperService>();
                x.SetServiceName("ImageKeeperService");
                x.SetDisplayName("Image Keeper Service");
                x.EnableServiceRecovery(recoveryConfigurator => 
                    recoveryConfigurator
                        .RestartService(firstRecoveryDelay)
                        .RestartService(secondRecoveryDelay));
                x.RunAsLocalService();
            });
        }
    }
}
