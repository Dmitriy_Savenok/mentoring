﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Task.Test")]

namespace Task
{
    internal class PdfDocumentBuilder
    {
        private Document _document;

        public PdfDocumentBuilder()
        {
            _document = new Document();
        }

        public PdfDocumentBuilder AddSection(string imagePath)
        {
            if (string.IsNullOrEmpty(imagePath))
            {
                throw new ArgumentNullException($"Argument {nameof(imagePath)} can not be null or empty");
            }

            var section = _document.AddSection();
            section.AddImage(imagePath);

            return this;
        }

        public void Save(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException($"Argument {nameof(path)} can not be null or empty");
            }

            var pdfRenderer = new PdfDocumentRenderer(false, PdfFontEmbedding.Always);
            pdfRenderer.Document = _document;
            pdfRenderer.RenderDocument();
            pdfRenderer.PdfDocument.Save(path);
        }

        public void Clear()
        {
            _document = new Document();
        }

        public bool IsEmpty()
        {
            return _document.Sections.Count == 0;
        }
    }
}
