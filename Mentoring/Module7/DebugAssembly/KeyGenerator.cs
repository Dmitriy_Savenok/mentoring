﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;

namespace DebugAssembly
{
    public class KeyGenerator
    {
        public string GenerateKey()
        {
            var builder = new StringBuilder();

            var networkInterface = ((IEnumerable<NetworkInterface>)NetworkInterface.GetAllNetworkInterfaces()).FirstOrDefault();
            var seedBytes = networkInterface.GetPhysicalAddress().GetAddressBytes();
            var binaryDate = BitConverter.GetBytes(DateTime.Now.Date.ToBinary());

            for (int i = 0; i < seedBytes.Length; i++)
            {
                var value = seedBytes[i] ^ binaryDate[i];

                if (value <= 999)
                {
                    value *= 10;
                }

                if (i != seedBytes.Length - 1)
                {
                    builder.Append($"{value}-");
                } 
                else
                {
                    builder.Append($"{value}");
                }
            }

            return builder.ToString();
        }
    }
}
