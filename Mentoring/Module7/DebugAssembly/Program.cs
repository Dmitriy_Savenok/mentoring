﻿using DebugAssembly;
using System;

namespace DebugConsole
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // the most important part of code for next research.
            //           IEnumerable<byte> arg_11B_0 = networkInterface.GetPhysicalAddress().GetAddressBytes();
            //           Form1.eval_a eval_a;
            //           eval_a.a = BitConverter.GetBytes(DateTime.Now.Date.ToBinary());
            //           IEnumerable<int> arg_140_0 = arg_11B_0.Select(new Func<byte, int, int>(eval_a.eval_a));
            //           Func<int, int> arg_140_1;
            //           if ((arg_140_1 = Form1.<> c.<> 9__3_1) == null)
            //{
            //               arg_140_1 = (Form1.<> c.<> 9__3_1 = new Func<int, int>(Form1.<> c.<> 9.eval_a));
            //           }
            //           IEnumerable<int> arg_173_0 = arg_140_0.Select(arg_140_1).ToArray<int>();
            //           eval_a.b = A_0.Select(new Func<string, int>(int.Parse)).ToArray<int>();
            //           IEnumerable<int> arg_1B4_0 = arg_173_0.Select(new Func<int, int, int>(eval_a.eval_a));
            //           Func<int, bool> arg_1B4_1;
            //           if ((arg_1B4_1 = Form1.<> c.<> 9__3_3) == null)
            //{
            //               arg_1B4_1 = (Form1.<> c.<> 9__3_3 = new Func<int, bool>(Form1.<> c.<> 9.eval_b));
            //           }
            //           return arg_1B4_0.All(arg_1B4_1);

            try
            {
                Console.WriteLine("Key is being generated.");

                var generator = new KeyGenerator();
                var result = generator.GenerateKey();

                Console.WriteLine("your key is:");
                Console.WriteLine(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong...");
                Console.WriteLine(ex.ToString());
            }

            Console.ReadLine();
        }
    }
}
