﻿using System.Runtime.InteropServices;
using System.Text;

namespace Task1
{
    [StructLayout(LayoutKind.Sequential)]
    [ComVisible(true)]
    public struct SystemBatteryState
    {
        public byte AcOnLine;
        public byte BatteryPresent;
        public byte Charging;
        public byte Discharging;
        public byte spare1;
        public byte spare2;
        public byte spare3;
        public byte spare4;
        public uint MaxCapacity;
        public uint RemainingCapacity;
        public int Rate;
        public uint EstimatedTime;
        public uint DefaultAlert1;
        public uint DefaultAlert2;

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.AppendLine($"AcOnLine: {AcOnLine}");
            builder.AppendLine($"BatteryPresent: {BatteryPresent}");
            builder.AppendLine($"Charging: {Charging}");
            builder.AppendLine($"Discharging: {Discharging}");
            builder.AppendLine($"spare1: {spare1}");
            builder.AppendLine($"spare2: {spare2}");
            builder.AppendLine($"spare3: {spare3}");
            builder.AppendLine($"spare4: {spare4}");
            builder.AppendLine($"MaxCapacity: {MaxCapacity}");
            builder.AppendLine($"RemainingCapacity: {RemainingCapacity}");
            builder.AppendLine($"Rate: {Rate}");
            builder.AppendLine($"EstimatedTime: {EstimatedTime}");
            builder.AppendLine($"DefaultAlert1: {DefaultAlert1}");
            builder.AppendLine($"DefaultAlert2: {DefaultAlert2}");

            return builder.ToString();
        }
    }
}
