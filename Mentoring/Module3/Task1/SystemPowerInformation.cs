﻿using System.Runtime.InteropServices;

namespace Task1
{
    [StructLayout(LayoutKind.Sequential)]
    [ComVisible(true)]
    public struct SystemPowerInformation
    {
        public uint MaxIdlenessAllowed;
        public uint Idleness;
        public uint TimeRemaining;
        public byte CoolingMode;

        public override string ToString()
        {
            return $"MaxIdlenessAllowed: {MaxIdlenessAllowed} Idleness: {Idleness} TimeRemaining: {TimeRemaining} CoolingMode: {CoolingMode}";
        }
    }
}
