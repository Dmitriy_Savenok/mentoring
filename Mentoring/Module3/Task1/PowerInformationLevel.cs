﻿namespace Task1
{
    internal enum PowerInformationLevel: uint
    {
        SystemBatteryState = 5,
        SystemReserveHiberFile = 10,
        SystemPowerInformation = 12,
        LastWakeTime = 14,
        LastSleepTime = 15,
    }
}
