﻿using System;
using System.Runtime.InteropServices;

namespace Task1
{
    internal static class PowrProfInterop
    {
        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern NtStatus CallNtPowerInformation(
            [In] PowerInformationLevel InformationLevel,
            [In] IntPtr lpInputBuffer,
            [In] uint nInputBufferSize,
            [Out] IntPtr lpOutputBuffer,
            [In] uint nOutputBufferSize);

        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern bool SetSuspendState(
            [In] bool hibernate,
            [In] bool forceCritical,
            [In] bool disableWakeEvent);
    }
}
