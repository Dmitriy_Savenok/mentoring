﻿using System;
using System.Runtime.InteropServices;

namespace Task1
{
    [ComVisible(true)]
    [Guid("1359295F-4672-4A8D-B5EC-05F98AB16B07")]
    [ClassInterface(ClassInterfaceType.None)]
    public class PowerInformationManager : IPowerInformationManager
    {
        public SystemBatteryState GetBatteryState()
        {
            return GetState<SystemBatteryState>(PowerInformationLevel.SystemBatteryState);
        }

        public ulong GetLastWakeTime()
        {
            return GetState<ulong>(PowerInformationLevel.LastWakeTime);
        }

        public ulong GetLastSleepTime()
        {
            return GetState<ulong>(PowerInformationLevel.LastSleepTime);
        }

        public SystemPowerInformation GetPowerInformation()
        {
            return GetState<SystemPowerInformation>(PowerInformationLevel.SystemPowerInformation);
        }

        public void ReserveHiberFile()
        {
            ManageHibernationFile(true);
        }

        public void RemoveHiberFile()
        {
            ManageHibernationFile(false);
        }

        public void HibernateSystem()
        {
            ManageSuspendState(true);
        }

        public void SuspendSystem()
        {
            ManageSuspendState(false);
        }

        private TResult GetState<TResult>(PowerInformationLevel level) where TResult : struct
        {
            var type = typeof(TResult);
            var result = Marshal.AllocCoTaskMem(Marshal.SizeOf(type));

            try
            {
                var ntStatus = PowrProfInterop.CallNtPowerInformation(level, IntPtr.Zero, 0, result, (uint)Marshal.SizeOf(type));

                if (ntStatus != NtStatus.Success)
                {
                    throw new Exception($"Expected status is {NtStatus.Success} but actual status is {ntStatus}");
                } 

                return (TResult)Marshal.PtrToStructure(result, type);
            }
            finally
            {
                Marshal.FreeCoTaskMem(result);
            }
        }

        private void ManageHibernationFile(bool isReserve)
        {
            var type = typeof(bool);
            var status = Marshal.AllocCoTaskMem(Marshal.SizeOf(type));
            Marshal.StructureToPtr(isReserve, status, false);

            try
            {
                var ntStatus = PowrProfInterop.CallNtPowerInformation(PowerInformationLevel.SystemReserveHiberFile, status, (uint)Marshal.SizeOf(type), IntPtr.Zero, 0);

                if (ntStatus != NtStatus.Success)
                {
                    throw new Exception($"Expected status is {NtStatus.Success} but actual status is {ntStatus}");
                }
            }
            finally
            {
                Marshal.FreeCoTaskMem(status);
            }
        }

        private void ManageSuspendState(bool isHibernated)
        {
            var result = PowrProfInterop.SetSuspendState(isHibernated, false, false);

            if (result == false)
            {
                throw new Exception("Something went wrong with SetSuspendState API");
            }
        }
    }
}
