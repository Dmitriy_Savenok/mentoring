﻿using System;
using System.Runtime.InteropServices;

namespace Task1
{
    [ComVisible(true)]
    [Guid("10710A43-DF94-47A5-87CC-C87667E06E28")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IPowerInformationManager
    {
        SystemBatteryState GetBatteryState();

        ulong GetLastWakeTime();

        ulong GetLastSleepTime();

        SystemPowerInformation GetPowerInformation();

        void ReserveHiberFile();

        void RemoveHiberFile();

        void HibernateSystem();

        void SuspendSystem();
    }
}
