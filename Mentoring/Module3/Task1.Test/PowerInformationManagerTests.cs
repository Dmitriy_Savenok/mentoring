﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Task1.Test
{
    [TestClass]
    public class PowerInformationManagerTests
    {
        [TestMethod]
        public void GetBatteryState_WhenBatteryStateIsRequired_ShouldReturnSystemBatteryState()
        {
            IPowerInformationManager manager = new PowerInformationManager();
            var result = manager.GetBatteryState();
            Console.WriteLine(result.ToString());
        }

        [TestMethod]
        public void GetLastSleepTime_WhenLastSleepTimeIsRequired_ShouldReturnLastSleepTime()
        {
            IPowerInformationManager manager = new PowerInformationManager();

            var result = manager.GetLastSleepTime();

            Console.WriteLine("The interrupt-time count, in 100-nanosecond units, at the last system sleep time.");
            Console.WriteLine(result);
        }

        [TestMethod]
        public void GetLastWakeTime_WhenLastWakeTimeIsRequired_ShouldReturnLastWakeTime()
        {
            IPowerInformationManager manager = new PowerInformationManager();
            
            var result = manager.GetLastWakeTime();

            Console.WriteLine("The interrupt-time count, in 100-nanosecond units, at the last system sleep time.");
            Console.WriteLine(result);
        }

        [TestMethod]
        public void GetPowerInformation_WhenPowerInformationIsRequired_ShouldReturnSystemPowerInformation()
        {
            IPowerInformationManager manager = new PowerInformationManager();
            var result = manager.GetPowerInformation();

            Console.WriteLine(result);
        }

        [TestMethod]
        [Ignore]
        public void ReserveHiberFile_WhenHiberFileIsRequired_ShouldReserveHiberFile()
        {
            IPowerInformationManager manager = new PowerInformationManager();
            manager.ReserveHiberFile();
        }

        [TestMethod]
        [Ignore]
        public void RemoveHiberFile_WhenHiberFileIsRequired_ShouldRemoveHiberFile()
        {
            IPowerInformationManager manager = new PowerInformationManager();
            manager.RemoveHiberFile();
        }

        [TestMethod]
        [Ignore]
        public void HibernateSystem_WhenHibernateSystemIsChecking_ShouldHibernateSystem()
        {
            IPowerInformationManager manager = new PowerInformationManager();
            manager.HibernateSystem();
        }

        [TestMethod]
        [Ignore]
        public void SuspendSystem_WhenSuspendSystemIsChecking_ShouldSuspendSystem()
        {
            IPowerInformationManager manager = new PowerInformationManager();
            manager.SuspendSystem();
        }
    }
}
