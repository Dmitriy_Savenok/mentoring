﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sample03.E3SClient.Entities;
using System;
using System.Configuration;
using System.Linq;

namespace Sample03
{
    [TestClass]
    public class E3SLinqProviderTests
    {
        [TestMethod]
        public void Where_WhenOperandsHaveReverseOrder_ShouldReturnEmployee()
        {
            var employees = new E3SEntitySet<EmployeeEntity>(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["password"]);

            foreach (var emp in employees.Where(e => "EPBYGOMW0516" == e.workstation))
            {
                Console.WriteLine($"Employee: {emp.firstName} {emp.lastName} Manager: {emp.manager}");
            }
        }

        [TestMethod]
        public void Where_WhenStartWithIsUsed_ShouldReturnEmployee()
        {
            var employees = new E3SEntitySet<EmployeeEntity>(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["password"]);

            foreach (var emp in employees.Where(e => e.workstation.StartsWith("EPBYGOMW05")))
            {
                Console.WriteLine($"Employee: {emp.firstName} {emp.lastName} Manager: {emp.manager}");
            }
        }

        [TestMethod]
        public void Where_WhenEndsWithIsUsed_ShouldReturnEmployee()
        {
            var employees = new E3SEntitySet<EmployeeEntity>(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["password"]);

            foreach (var emp in employees.Where(e => e.workstation.EndsWith("GOMW0516")))
            {
                Console.WriteLine($"Employee: {emp.firstName} {emp.lastName} Manager: {emp.manager}");
            }
        }

        [TestMethod]
        public void Where_WhenContainsIsUsed_ShouldReturnEmployee()
        {
            var employees = new E3SEntitySet<EmployeeEntity>(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["password"]);

            foreach (var emp in employees.Where(e => e.workstation.Contains("BYGOMW0")))
            {
                Console.WriteLine($"Employee: {emp.firstName} {emp.lastName} Manager: {emp.manager}");
            }
        }

        [TestMethod]
        public void Where_WhenAndOperatorIsUsed_ShouldReturnEmployee()
        {
            var employees = new E3SEntitySet<EmployeeEntity>(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["password"]);

            foreach (var emp in employees.Where(e => e.workstation.StartsWith("EPBYGOMW05") && e.lastName == "Savianok"))
            {
                Console.WriteLine($"Employee: {emp.firstName} {emp.lastName} Manager: {emp.manager}");
            }
        }

        [TestMethod]
        public void Where_WhenAndOperatorIsUsedTwice_ShouldReturnEmployee()
        {
            var employees = new E3SEntitySet<EmployeeEntity>(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["password"]);

            foreach (var emp in employees.Where(e => e.workstation.StartsWith("EPBYGOMW05") && e.lastName == "Savianok" && e.firstName == "Dzmitry"))
            {
                Console.WriteLine($"Employee: {emp.firstName} {emp.lastName} Manager: {emp.manager}");
            }
        }

        [TestMethod]
        public void Where_WhenAndOperatorIsUsedAndHasAnotherOrder_ShouldReturnEmployee()
        {
            var employees = new E3SEntitySet<EmployeeEntity>(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["password"]);

            foreach (var emp in employees.Where(e => e.lastName == "Savianok" && e.workstation.StartsWith("EPBYGOMW05")))
            {
                Console.WriteLine($"Employee: {emp.firstName} {emp.lastName} Manager: {emp.manager}");
            }
        }
    }
}
