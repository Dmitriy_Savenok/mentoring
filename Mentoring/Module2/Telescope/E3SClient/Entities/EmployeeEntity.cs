﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Sample03.E3SClient.Entities
{
    [E3SMetaType("meta:people-suite:people-api:com.epam.e3s.app.people.api.data.pluggable.EmployeeEntity")]
	public class EmployeeEntity : E3SEntity
	{
		[JsonProperty]
        public double entityBoost { get; set; }

		[JsonProperty]
        public double documentBoost { get; set; }

		[JsonProperty]
        public List<string> phone;

		[JsonProperty]
        public Skills skill { get; set; }

		[JsonProperty]
        public string firstName { get; set; }

		[JsonProperty]
		public string lastName { get; set; }

		[JsonProperty]
        public List<string> fullname { get; set; }

		[JsonProperty]
        public List<string> country { get; set; }

		[JsonProperty]
        public List<string> city { get; set; }

		[JsonProperty]
        public List<string> email { get; set; }

		[JsonProperty]
        public List<string> skype { get; set; }

		[JsonProperty]
        public List<string> socialNetwork { get; set; }

		[JsonProperty]
        public List<string> attachment { get; set; }

		[JsonProperty]
		public string manager { get; set; }

		[JsonProperty]
		public string superior { get; set; }

        [JsonProperty]
        public string startworkdate { get; set; }

        [JsonProperty]
		public string project { get; set; }

		[JsonProperty]
		public string projectall { get; set; }

		[JsonProperty]
		List<string> trainer { get; set; }

		[JsonProperty]
        public List<string> kb { get; set; }

		[JsonProperty]
		public string certificate { get; set; }

		[JsonProperty]
		public string unit { get; set; }

		[JsonProperty]
		public string office { get; set; }

		[JsonProperty]
		public string room { get; set; }

		[JsonProperty]
		public string status { get; set; }

		[JsonProperty]
		public string car { get; set; }

		[JsonProperty]
		public string birthday { get; set; }

		[JsonProperty]
		public List<WorkHistory> workhistory { get; set; }

        [JsonProperty]
        public List<Badge> badge { get; set; }

        [JsonProperty]
        public List<string> jobfunction { get; set; }

		[JsonProperty]
        public List<Recognition> recognition { get; set; }

		[JsonProperty]
		public string dismissal { get; set; }

		[JsonProperty]
		public string endProbationDate { get; set; }

		[JsonProperty]
		public string endworkdate { get; set; }

		[JsonProperty]
		public string errupdatedate { get; set; }

		[JsonProperty]
		public string edulevel { get; set; }

		[JsonProperty]
		public string eduschool { get; set; }

		[JsonProperty]
		public string edufield { get; set; }

		[JsonProperty]
		public string eduendyear { get; set; }

        [JsonProperty]
        public string workstation { get; set; }

        [JsonProperty]
        public string nativename { get; set; }

        [JsonProperty]
		public string governmentalid { get; set; }

		[JsonProperty]
		public double billable { get; set; }

		[JsonProperty]
		public double nonbillable { get; set; }
	}
}
