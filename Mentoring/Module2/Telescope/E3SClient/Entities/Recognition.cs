﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample03.E3SClient.Entities
{
    public class Recognition
    {
        [JsonProperty]
        public string nomination { get; set; }

        [JsonProperty]
        public string description { get; set; }

        [JsonProperty]
        public string recognitiondate { get; set; }

        [JsonProperty]
        public string points { get; set; }
    }
}
