﻿using Newtonsoft.Json;
using System;

namespace Sample03.E3SClient.Entities
{
    public class Badge
    {
        [JsonProperty]
        public string id { get; set; }

        [JsonProperty]
        public string image { get; set; }

        [JsonProperty]
        public string date { get; set; }

        [JsonProperty]
        public string name { get; set; }

        [JsonProperty]
        public string category { get; set; }

        [JsonProperty]
        public bool e3sImageExists { get; set; }
    }
}
