﻿using System;
using System.Linq.Expressions;
using System.Text;

namespace Sample03
{
    public enum StringMethodType
    {
        Unknown,
        StartsWith,
        EndsWith,
        Contains
    }

    public class StringExpressionVisitor : ExpressionVisitor
    {
        private StringMethodType methodType;
        private StringBuilder resultBuilder;

        public void Modify(Expression node)
        {
            resultBuilder = new StringBuilder();
            base.Visit(node);
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            switch (node.Method.Name)
            {
                case "StartsWith":
                    methodType = StringMethodType.StartsWith;
                    break;
                case "EndsWith":
                    methodType = StringMethodType.EndsWith;
                    break;
                case "Contains":
                    methodType = StringMethodType.Contains;
                    break;
                default:
                    throw new NotSupportedException(string.Format("Operation {0} is not supported", node.NodeType));
            }

            return base.VisitMethodCall(node);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            resultBuilder.Append(node.Member.Name).Append(":");

            return base.VisitMember(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            var value = string.Empty;

            switch (methodType)
            {
                case StringMethodType.StartsWith:
                    value = $"({node.Value}*)";
                    break;
                case StringMethodType.EndsWith:
                    value = $"(*{node.Value})";
                    break;
                case StringMethodType.Contains:
                    value = $"(*{node.Value}*)";
                    break;
                default:
                    throw new NotSupportedException(string.Format("Operation {0} is not supported", node.NodeType));
            }

            resultBuilder.Append(value);

            return base.VisitConstant(node);
        }

        public string Result()
        {
            if (resultBuilder != null && resultBuilder.Length != 0)
            {
                return resultBuilder.ToString();
            }

            return string.Empty;
        }
    }
}
