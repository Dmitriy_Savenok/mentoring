﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sample03.E3SClient.Entities;
using Sample03.E3SClient;
using System.Configuration;
using System.Linq;

namespace Sample03
{
	[TestClass]
	public class E3SProviderTests
	{
		[TestMethod]
		public void WithoutProvider()
		{
            var workstation = ConfigurationManager.AppSettings["workstation"];
            var user = ConfigurationManager.AppSettings["user"];
            var password = ConfigurationManager.AppSettings["password"];

            var client = new E3SQueryClient(user, password);
			var res = client.SearchFTS<EmployeeEntity>($"workstation:({workstation})", 0, 1);

            foreach (var emp in res)
			{
				Console.WriteLine($"Employee: {emp.firstName} {emp.lastName} Manager: {emp.manager}");
			}
		}

		[TestMethod]
		public void WithoutProviderNonGeneric()
		{
            var workstation = ConfigurationManager.AppSettings["workstation"];
            var user = ConfigurationManager.AppSettings["user"];
            var password = ConfigurationManager.AppSettings["password"];

            var client = new E3SQueryClient(user, password);
			var res = client.SearchFTS(typeof(EmployeeEntity), $"workstation:({workstation})", 0, 10);

			foreach (var emp in res.OfType<EmployeeEntity>())
			{
                Console.WriteLine($"Employee: {emp.firstName} {emp.lastName} Manager: {emp.manager}");
            }
		}


		[TestMethod]
		public void WithProvider()
		{
            var employees = new E3SEntitySet<EmployeeEntity>(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["password"]);

            foreach (var emp in employees.Where(e => e.workstation == "EPBYGOMW0516"))
            {
                Console.WriteLine($"Employee: {emp.firstName} {emp.lastName} Manager: {emp.manager}");
            }
        }
	}
}
