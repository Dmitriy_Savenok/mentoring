﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Sample03
{
    public class ExpressionToFTSRequestTranslator : ExpressionVisitor
    {
        private StringBuilder resultString;
        private StringExpressionVisitor stringVisitor = new StringExpressionVisitor();
        private bool isSkippedVisit = false;

        public string Translate(Expression exp)
        {
            resultString = new StringBuilder();
            Visit(exp);

            return resultString.ToString();
        }

        private bool isReverse(BinaryExpression node)
        {
            var result = false;

            if ((node.Right.NodeType == ExpressionType.MemberAccess) && (node.Left.NodeType == ExpressionType.Constant))
            {
                result = true;               
       
            } else if (!((node.Left.NodeType == ExpressionType.MemberAccess) && (node.Right.NodeType == ExpressionType.Constant)))
                throw new NotSupportedException("Incorrect operands are used.");

            return result;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.DeclaringType == typeof(Queryable) && node.Method.Name == "Where")
            {
                var predicate = node.Arguments[1];

                if (!isSkippedVisit)
                {
                    Visit(predicate);
                    isSkippedVisit = true;
                }                
            }
            else if (node.Method.DeclaringType == typeof(string))
            {                
                if (!isSkippedVisit)
                {
                    stringVisitor.Modify(node);
                    resultString.Append(stringVisitor.Result());
                    isSkippedVisit = true;
                }
            }

            return node;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Equal:

                    if (!isReverse(node))
                    {
                        Visit(node.Left);
                        resultString.Append("(");
                        Visit(node.Right);
                        resultString.Append(")");
                    }
                    else
                    {
                        Visit(node.Right);
                        resultString.Append("(");
                        Visit(node.Left);
                        resultString.Append(")");
                    }

                    isSkippedVisit = true;

                    break;

                case ExpressionType.AndAlso:
                    Visit(node.Left);

                    isSkippedVisit = false;

                    resultString.Append("+");
                    Visit(node.Right);

                    break;

                default:
                    throw new NotSupportedException(string.Format("Operation {0} is not supported", node.NodeType));
            };

            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            if (isSkippedVisit)
                return base.VisitMember(node);

            resultString.Append(node.Member.Name).Append(":");

            return base.VisitMember(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            if (isSkippedVisit)
                return base.VisitConstant(node);

            resultString.Append(node.Value);

            return base.VisitConstant(node);
        } 
    }
}
