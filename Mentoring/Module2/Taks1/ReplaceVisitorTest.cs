﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;

namespace Taks1
{
    [TestClass]
    public class ReplaceVisitorTest
    {
        [TestMethod]
        public void Modify_WhenExpressionHasParameter_ShouldReplace()
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("b", 15);

            Expression<Func<int, int>> expression = (b) => b + 1;            

            Console.WriteLine(expression.ToString());

            var visitor = new ReplaceVisitor();
            var modifiedExp = visitor.Modify(expression, dictionary);

            Console.WriteLine(modifiedExp.ToString());
            Assert.AreEqual("b => (15 + 1)", modifiedExp.ToString());
        }

        [TestMethod]
        public void Modify_WhenExpressionHasThreeParameters_ShouldReplaceEach()
        {
            var valueB = 15;
            var valueC = 99.8798d;
            var valueD = -117;
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("b", valueB);
            dictionary.Add("c", valueC);
            dictionary.Add("d", valueD);

            Expression<Func<int, double, int, double>> expression = (b, c, d) => (b + 1222) + (c * 123) - (d / 2);

            Console.WriteLine(expression.ToString());

            var visitor = new ReplaceVisitor();
            var modifiedExp = visitor.Modify(expression, dictionary);

            Console.WriteLine(modifiedExp.ToString());
            Assert.AreEqual($"(b, c, d) => ((Convert(({valueB.ToString(CultureInfo.InvariantCulture)} + 1222)) + ({valueC.ToString()} * 123)) - Convert(({valueD.ToString()} / 2)))", modifiedExp.ToString());
        }

        [TestMethod]
        public void Modify_WhenExpressionDoesNotHaveParameter_ShouldNotReplace()
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("b", 17);

            Expression<Func<int>> expression = () => 15;

            Console.WriteLine(expression.ToString());

            var visitor = new ReplaceVisitor();
            var modifiedExp = visitor.Modify(expression, dictionary);

            Console.WriteLine(modifiedExp.ToString());
            Assert.AreEqual("() => 15", modifiedExp.ToString());
        }
    }
}
