﻿using System;
using System.Linq.Expressions;

namespace Taks1
{
    public class UnaryVisitor : ExpressionVisitor
    {
        private string expectedValue = "1";

        public Expression Modify(Expression expression)
        {
            return Visit(expression);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            if (node.NodeType == ExpressionType.Add || node.NodeType == ExpressionType.Subtract)
            {
                var left = Visit(node.Left);
                var right = Visit(node.Right);

                if (right.NodeType == ExpressionType.Constant)
                {
                    var constant = (ConstantExpression)right;
                    if (constant.Type.IsValueType && expectedValue.Equals(constant.Value.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        var type = node.NodeType == ExpressionType.Add ? ExpressionType.UnaryPlus : ExpressionType.Negate;
                        return Expression.MakeUnary(type, node.Left, node.Left.Type);
                    }
                }

                return base.VisitBinary(node);
            }

            return base.VisitBinary(node);
        }


    }
}
