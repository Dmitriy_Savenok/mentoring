﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;

namespace Taks1
{
    [TestClass]
    public class UnaryVisitorTests
    {
        [TestMethod]
        public void Modify_WhenExpressionHasCorrectBody_ShouldReplaceOnUnaryExpression()
        {
            Expression<Func<int, int>> exp = (b) => b + 1;
            Console.WriteLine(exp.ToString());

            var visitor = new UnaryVisitor();
            var modifiedExp = visitor.Modify(exp);

            Console.WriteLine(modifiedExp.ToString());
            Assert.AreEqual("b => +b", modifiedExp.ToString());
        }

        [TestMethod]
        public void Modify_WhenExpressionHasIncorrectBody_ShouldNotReplaceOnUnaryExpression()
        {
            Expression<Func<int, int>> exp = (b) => b + 5;
            Console.WriteLine(exp.ToString());

            var visitor = new UnaryVisitor();
            var modifiedExp = visitor.Modify(exp);

            Console.WriteLine(modifiedExp.ToString());
            Assert.AreEqual("b => (b + 5)", modifiedExp.ToString());
        }

        [TestMethod]
        public void Modify_WhenExpressionHasCorrectBody_ShouldReplaceOnNegativeUnaryExpression()
        {
            Expression<Func<int, int>> exp = (b) => b - 1;
            Console.WriteLine(exp.ToString());

            var visitor = new UnaryVisitor();
            var modifiedExp = visitor.Modify(exp);

            Console.WriteLine(modifiedExp.ToString());
            Assert.AreEqual("b => -b", modifiedExp.ToString());
        }

        [TestMethod]
        public void Modify_WhenExpressionHasIncorrectBody_ShouldNotReplaceOnNegativeUnaryExpression()
        {
            Expression<Func<int, int>> exp = (b) => b - 10;
            Console.WriteLine(exp.ToString());

            var visitor = new UnaryVisitor();
            var modifiedExp = visitor.Modify(exp);

            Console.WriteLine(modifiedExp.ToString());
            Assert.AreEqual("b => (b - 10)", modifiedExp.ToString());
        }
    }
}
