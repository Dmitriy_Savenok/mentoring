﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Taks1
{
    public class ReplaceVisitor : ExpressionVisitor
    {
        public Dictionary<string, object> valueDictionary = new Dictionary<string, object>();

        public Expression Modify(Expression expression, Dictionary<string, object> valueDictionary)
        {            
            if (valueDictionary == null && valueDictionary.Count == 0)
            {
                throw new ArgumentNullException($"Argument {nameof(valueDictionary)} can not be null or empty");
            }

            this.valueDictionary = valueDictionary;

            return Visit(expression);
        }

        private Expression GetReplacement(Expression node)
        {
            if (node.NodeType == ExpressionType.Parameter)
            {
                var parameter = (ParameterExpression)node;

                var value = valueDictionary[parameter.Name];

                if(value == null)
                {
                    throw new ArgumentOutOfRangeException($"Dictionaty {nameof(valueDictionary)} have to contain correct definition for {parameter.Name}");
                }

                return Expression.Constant(value, parameter.Type);
            }

            return null;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            var left = Visit(node.Left);
            var right = Visit(node.Right);

            var leftReplacement = GetReplacement(left);
            var rightReplacement = GetReplacement(right);

            if (right != null || left != null)
            {
                return Expression.MakeBinary(node.NodeType, leftReplacement ?? left, rightReplacement ?? right);
            }

            return base.VisitBinary(node);
        }
    }
}
