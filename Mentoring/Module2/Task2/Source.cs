﻿namespace Task2
{
    public class Source
    {
        public int Field1;

        public float Field2;

        public double Field3;

        public string Field4;

        public long Field5;

        public int Property1 { get; set; }

        public float Property2 { get; set; }

        public double Property3 { get; set; }

        public string Property4 { get; set; }

        public long Property5 { get; set; }
    }
}
