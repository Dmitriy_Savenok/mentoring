﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Task2
{
    [TestClass]
    public class MapperTest
    {
        [TestMethod]
        public void Map_WhenSourceIsDestination_ShouldMapToDestination()
        {
            var mapGenerator = new MappingGenerator();
            var mapper = mapGenerator.Generate<Source, Source>();

            var source = new Source();
            source.Property1 = 1;
            source.Property2 = 2.56f;
            source.Property3 = 983.234234;
            source.Property4 = "abcdefg";
            source.Property5 = 135467;

            source.Field1 = 2;
            source.Field2 = 89.56f;
            source.Field3 = -12.33;
            source.Field4 = "kjhfdf";
            source.Field5 = 693;

            var destination = mapper.Map(source);

            Assert.AreEqual(source.Property1, destination.Property1);
            Assert.AreEqual(source.Property2, destination.Property2);
            Assert.AreEqual(source.Property3, destination.Property3);
            Assert.AreEqual(source.Property4, destination.Property4);
            Assert.AreEqual(source.Property5, destination.Property5);

            Assert.AreEqual(source.Field1, destination.Field1);
            Assert.AreEqual(source.Field2, destination.Field2);
            Assert.AreEqual(source.Field3, destination.Field3);
            Assert.AreEqual(source.Field4, destination.Field4);
            Assert.AreEqual(source.Field5, destination.Field5);
        }

        [TestMethod]
        public void Map_WhenSourceHasFiledsAndPropertiesLikeInDestination_ShouldMapToDestination()
        {
            var mapGenerator = new MappingGenerator();
            var mapper = mapGenerator.Generate<Source, RichDestination>();

            var source = new Source();
            source.Property1 = 1;
            source.Property2 = 2.56f;
            source.Property3 = 983.234234;
            source.Property4 = "abcdefg";
            source.Property5 = 135467;

            source.Field1 = 2;
            source.Field2 = 89.56f;
            source.Field3 = -12.33;
            source.Field4 = "kjhfdf";
            source.Field5 = 693;

            var destination = mapper.Map(source);

            Assert.AreEqual(source.Property1, destination.Property1);
            Assert.AreEqual(source.Property2, destination.Property2);
            Assert.AreEqual(source.Property3, destination.Property3);
            Assert.AreEqual(source.Property4, destination.Property4);
            Assert.AreEqual(source.Property5, destination.Property5);

            Assert.AreEqual(source.Field1, destination.Field1);
            Assert.AreEqual(source.Field2, destination.Field2);
            Assert.AreEqual(source.Field3, destination.Field3);
            Assert.AreEqual(source.Field4, destination.Field4);
            Assert.AreEqual(source.Field5, destination.Field5);
        }

        [TestMethod]
        public void Map_WhenSourceHasFiledsAndPropertiesGreaterThenInDestination_ShouldMapToDestination()
        {
            var mapGenerator = new MappingGenerator();
            var mapper = mapGenerator.Generate<Source, PoorDestination>();
            
            var source = new Source();
            source.Property1 = 1;
            source.Property2 = 2.56f;
            source.Property3 = 983.234234;
            source.Property4 = "abcdefg";
            source.Property5 = 135467;

            source.Field1 = 2;
            source.Field2 = 89.56f;
            source.Field3 = -12.33;
            source.Field4 = "kjhfdf";
            source.Field5 = 693;

            var destination = mapper.Map(source);

            Assert.AreEqual(source.Property1, destination.Property1);
            Assert.AreEqual(source.Property3, destination.Property3);
            Assert.AreEqual(source.Property5, destination.Property5);

            Assert.AreEqual(source.Field1, destination.Field1);
            Assert.AreEqual(source.Field3, destination.Field3);
            Assert.AreEqual(source.Field5, destination.Field5);
        }
    }
}
