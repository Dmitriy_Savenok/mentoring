﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Task2
{
    public class MappingGenerator
    {
        public Mapper<TSource, TDestination> Generate<TSource, TDestination>()
        {
            var sourceParameter = Expression.Parameter(typeof(TSource), "source");

            var memberBindings = new List<MemberBinding>();
            memberBindings.AddRange(GetFieldBindings<TSource, TDestination>(sourceParameter));
            memberBindings.AddRange(GetPropertyBindings<TSource, TDestination>(sourceParameter));

            var body = Expression.MemberInit(Expression.New(typeof(TDestination)), memberBindings);

            var mapFunction = Expression.Lambda<Func<TSource, TDestination>>(body, sourceParameter);

            return new Mapper<TSource, TDestination>(mapFunction.Compile());
        }

        private List<MemberBinding> GetFieldBindings<TSource, TDestination>(ParameterExpression sourceParameter)
        {
            var destinationFields = typeof(TDestination).GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            var sourceFileds = typeof(TSource).GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            var bindings = new List<MemberBinding>(destinationFields.Length);

            foreach (var field in destinationFields)
            {
                if (sourceFileds.Any(x => x.Name == field.Name && x.FieldType == field.FieldType))
                {
                    var member = field as MemberInfo;
                    var readMemberExpression = Expression.PropertyOrField(sourceParameter, member.Name);
                    bindings.Add(Expression.Bind(member, readMemberExpression));
                }
            }

            return bindings;
        }

        private List<MemberBinding> GetPropertyBindings<TSource, TDestination>(ParameterExpression sourceParameter)
        {
            var destinationProperties = typeof(TDestination).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            var sourceProperties = typeof(TDestination).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            var bindings = new List<MemberBinding>(destinationProperties.Length);

            foreach (var property in destinationProperties)
            {
                if (sourceProperties.Any(x => x.Name == property.Name && x.PropertyType == property.PropertyType))
                {
                    var member = property as MemberInfo;
                    var readMemberExpression = Expression.PropertyOrField(sourceParameter, member.Name);
                    bindings.Add(Expression.Bind(member, readMemberExpression));
                }
            }

            return bindings;
        }
    }
}
