﻿namespace Task2.Forms
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.downloadButton = new System.Windows.Forms.Button();
            this.settingPanel = new System.Windows.Forms.Panel();
            this.settingPanelLabel = new System.Windows.Forms.Label();
            this.openFolderButton = new System.Windows.Forms.Button();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.sourceDataGridView = new System.Windows.Forms.DataGridView();
            this.sourceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cancelButtonColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.settingPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // downloadButton
            // 
            this.downloadButton.Location = new System.Drawing.Point(221, 285);
            this.downloadButton.Name = "downloadButton";
            this.downloadButton.Size = new System.Drawing.Size(75, 23);
            this.downloadButton.TabIndex = 0;
            this.downloadButton.Text = "Download";
            this.downloadButton.UseVisualStyleBackColor = true;
            this.downloadButton.Click += new System.EventHandler(this.downloadButton_Click);
            // 
            // settingPanel
            // 
            this.settingPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.settingPanel.Controls.Add(this.settingPanelLabel);
            this.settingPanel.Controls.Add(this.openFolderButton);
            this.settingPanel.Controls.Add(this.pathTextBox);
            this.settingPanel.Location = new System.Drawing.Point(12, 12);
            this.settingPanel.Name = "settingPanel";
            this.settingPanel.Size = new System.Drawing.Size(501, 75);
            this.settingPanel.TabIndex = 1;
            // 
            // settingPanelLabel
            // 
            this.settingPanelLabel.AutoSize = true;
            this.settingPanelLabel.Location = new System.Drawing.Point(7, 11);
            this.settingPanelLabel.Name = "settingPanelLabel";
            this.settingPanelLabel.Size = new System.Drawing.Size(40, 13);
            this.settingPanelLabel.TabIndex = 2;
            this.settingPanelLabel.Text = "Setting";
            // 
            // openFolderButton
            // 
            this.openFolderButton.Location = new System.Drawing.Point(418, 37);
            this.openFolderButton.Name = "openFolderButton";
            this.openFolderButton.Size = new System.Drawing.Size(75, 23);
            this.openFolderButton.TabIndex = 1;
            this.openFolderButton.Text = "Open";
            this.openFolderButton.UseVisualStyleBackColor = true;
            this.openFolderButton.Click += new System.EventHandler(this.openFolderButton_Click);
            // 
            // pathTextBox
            // 
            this.pathTextBox.Location = new System.Drawing.Point(10, 40);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(402, 20);
            this.pathTextBox.TabIndex = 0;
            this.pathTextBox.Text = "E:\\";
            // 
            // sourceDataGridView
            // 
            this.sourceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sourceDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sourceColumn,
            this.cancelButtonColumn});
            this.sourceDataGridView.Location = new System.Drawing.Point(12, 93);
            this.sourceDataGridView.Name = "sourceDataGridView";
            this.sourceDataGridView.Size = new System.Drawing.Size(501, 177);
            this.sourceDataGridView.TabIndex = 4;
            this.sourceDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.sourceDataGridView_CellClick);
            // 
            // sourceColumn
            // 
            this.sourceColumn.HeaderText = "Source";
            this.sourceColumn.Name = "sourceColumn";
            this.sourceColumn.Width = 355;
            // 
            // cancelButtonColumn
            // 
            this.cancelButtonColumn.HeaderText = "Cancel";
            this.cancelButtonColumn.Name = "cancelButtonColumn";
            this.cancelButtonColumn.Text = "";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 319);
            this.Controls.Add(this.sourceDataGridView);
            this.Controls.Add(this.settingPanel);
            this.Controls.Add(this.downloadButton);
            this.Name = "Main";
            this.Text = "Loader";
            this.settingPanel.ResumeLayout(false);
            this.settingPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button downloadButton;
        private System.Windows.Forms.Panel settingPanel;
        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button openFolderButton;
        private System.Windows.Forms.Label settingPanelLabel;
        private System.Windows.Forms.DataGridView sourceDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn sourceColumn;
        private System.Windows.Forms.DataGridViewButtonColumn cancelButtonColumn;
    }
}

