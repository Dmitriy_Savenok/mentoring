﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task2.Forms
{
    public partial class Main : Form
    {
        private int _cancelColumnIndex = 1;
        private int _sourceColumnIndex = 0;

        private ILoader _loader = new Loader();
        private Dictionary<int, CancellationTokenSource> _tokeSourceDictionaty = new Dictionary<int, CancellationTokenSource>();

        public Main()
        {
            InitializeComponent();
        }

        private Setting getSetting()
        {
            var setting = new Setting();

            setting.Path = pathTextBox.Text;

            return setting;
        }

        private async void downloadButton_Click(object sender, EventArgs e)
        {
            _loader.Init(getSetting());

            var tasks = new List<Task>();
            for (int i = 0; i < sourceDataGridView.RowCount; i++)
            {                
                var source = sourceDataGridView.Rows[i].Cells[_sourceColumnIndex].Value?.ToString();

                var tokenSource = new CancellationTokenSource();
                _tokeSourceDictionaty.Add(i, tokenSource);

                tasks.Add(_loader.LoadAsync(source, tokenSource.Token));
            }            

            await Task.WhenAll(tasks);

            _tokeSourceDictionaty.Clear();
        }

        private void openFolderButton_Click(object sender, EventArgs e)
        {
            var result = folderBrowserDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                pathTextBox.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void sourceDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == _cancelColumnIndex)
            {
                _tokeSourceDictionaty[e.RowIndex].Cancel();
            }
        }
    }
}
