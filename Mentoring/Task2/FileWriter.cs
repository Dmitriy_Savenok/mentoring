﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Task2
{
    public class FileWriter : IFileWriter
    {
        private int _bufferSize = 1024;

        public async Task SaveFileAsync(Stream contentStream, string fileName, CancellationToken token)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException($"Argemtn {nameof(fileName)} can not be null or empty");
            }

            using (var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None, _bufferSize, true))
            {                
                await contentStream.CopyToAsync(stream, _bufferSize, token);
            }
        }
    }
}
