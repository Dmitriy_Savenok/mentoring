﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Task2
{
    public interface ILoader
    {
        void Init(Setting setting);

        Task LoadAsync(string uri, CancellationToken token);
    }
}
