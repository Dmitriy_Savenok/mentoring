﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Task2
{
    public interface IFileWriter
    {
        Task SaveFileAsync(Stream contentStream, string fileName, CancellationToken token);
    }
}
