﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Task2
{
    public class Loader : ILoader, IDisposable
    {
        private Setting _setting;
        private HttpClient _client;
        private IFileWriter _writer;
        private bool _isInitialized;

        public Loader()
        {
            _writer = new FileWriter();
            _client = new HttpClient();
        }

        private string getFileName(Uri requestUri)
        {
            var fileName = requestUri.Host.Replace('.', '_') + requestUri.LocalPath.Replace('/', '_') + ".html";
            return Path.Combine(_setting.Path, fileName);
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public void Init(Setting setting)
        {
            if (setting == null)
            {
                throw new ArgumentNullException($"Argument {nameof(setting)} can not be null");
            }
            else if (string.IsNullOrEmpty(setting.Path))
            {
                throw new ArgumentNullException($"Argemtn {nameof(setting.Path)} can not be null or empty");
            }

            _isInitialized = true;
            _setting = setting;
        }

        public async Task LoadAsync(string uri, CancellationToken token)
        {
            try
            {
                if (!_isInitialized)
                {
                    throw new InvalidOperationException($"Current instance should be initialized before using");
                }

                Uri downloadFromUri;

                if (Uri.TryCreate(uri, UriKind.Absolute, out downloadFromUri))
                {
                    var response = await _client.GetAsync(uri, token);
                    var stream = await response.Content.ReadAsStreamAsync();

                    await _writer.SaveFileAsync(stream, getFileName(response.RequestMessage.RequestUri), token);
                }
            }
            catch (OperationCanceledException) { }
        }
    }
}
